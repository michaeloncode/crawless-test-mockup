import React from 'react';
import { MemoryRouter as Router, Routes, Route } from 'react-router-dom';

import MainScreen from './screens/MainScreen';

export default function AppRouter() {
  return (
    <Router>
      <Routes>
        <Route
          path="/"
          element={<MainScreen />}
        />
      </Routes>
    </Router>
  );
}
