import React, { useState } from 'react';
import { DEFAULT_TABS, WORKFLOW } from 'renderer/dataset';
import { Tabs, Svg } from '@crawless/ui/lite';

import Workflow from './workflow';

const ProjectsRenderer = () => {
  const renderPaneContent = (tab) => {
    if (tab?.type === WORKFLOW) {
      return <Workflow />;
    }
    return <div>Hello World</div>;
  };

  return (
    <Tabs
      size="small"
      onChange={() => {}}
      // defaultActiveId={tabsData.getActiveTab()}
      activeId="4"
    >
      {DEFAULT_TABS.map((el, key) => (
        <Tabs.Pane
          key={key}
          id={el?.id}
          title={
            <div className="d-flex align-items-center pe-2">
              <span className="text-capitalize">{el?.title}</span>
              <Svg.Cross style={{ marginLeft: '10px' }} />
            </div>
          }
          iconLeft={
            el?.Icon ? (
              <el.Icon style={el?.color ? { fill: el?.color } : {}} />
            ) : null
          }
        >
          {renderPaneContent(el)}
        </Tabs.Pane>
      ))}
    </Tabs>
  );
};

export default ProjectsRenderer;
