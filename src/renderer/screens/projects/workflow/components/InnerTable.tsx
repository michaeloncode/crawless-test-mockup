/* eslint-disable no-extra-boolean-cast */
import React from 'react';
import styled from 'styled-components';
import { Checkbox, Badge, Link } from '@crawless/ui/lite';
import { Colors } from 'renderer/theme';
import dayjs from 'dayjs';
import advancedFormat from 'dayjs/plugin/advancedFormat';
dayjs.extend(advancedFormat);

const PurpleText = styled.div`
  color: ${Colors.purple50}!important;
`;

const WhiteText = styled.div`
  color: ${Colors.foreground70};
`;
const RedText = styled.div`
  color: ${Colors.red50};
`;

const InnerTable = ({ linesData, ...props }) => {
  return (
    Array.isArray(linesData) &&
    linesData.map((el) => {
      return (
        <tr key={el?.id}>
          <td style={{ width: '35px' }}>
            <Checkbox />
          </td>
          <td style={{ minWidth: '80px' }}>
            <Badge type={el?.status} />
          </td>
          <td style={{ minWidth: '80px' }}>
            <WhiteText>{dayjs(el?.createdAt).format('HH:mm:ss')}</WhiteText>
          </td>
          <td style={{ minWidth: '80px' }}>
            <PurpleText>{el?.name}</PurpleText>
          </td>
          <td style={{ minWidth: '150px' }}>
            <Link
              style={{ color: Colors.purple50 }}
              href={el?.url}
              size="link25"
            >
              {el?.url}
            </Link>
          </td>
          <td style={{ minWidth: '80px' }}>
            <WhiteText>{el?.retries}</WhiteText>
          </td>
          <td>
            <RedText>{el?.error}</RedText>
          </td>
          <td style={{ minWidth: '80px' }}>
            <img
              src={el?.screenshot}
              style={{
                width: '20px',
                height: '20px',
              }}
              alt={el?.name}
            />
          </td>
          <td style={{ minWidth: '80px' }}>
            <Badge type={`${el?.visible}`} />
          </td>
          <td style={{ minWidth: '80px' }}>
            <WhiteText>
              {!!el?.timeout
                ? dayjs(el?.timeout, 'X').format('H[h],mm[mn]')
                : ''}
            </WhiteText>
          </td>
          <td colSpan={2} style={{ minWidth: '80px' }}>
            <Badge type={el?.method} />
          </td>
        </tr>
      );
    })
  );
};

export default InnerTable;
