import React from 'react';
import { Svg, Tabs, Tooltip } from '@crawless/ui/lite';
import { Colors } from 'renderer/theme';
import styled from 'styled-components';
import SummaryContent from './SummaryContent';

const EdgeContent = () => {
  return (
    <Tabs
      position="left"
      paneStyle={{
        padding: '0',
      }}
      panesStyle={{
        background: Colors.background70,
      }}
      style={{ height: 'calc(100vh - 75px)' }}
      prefix={
        <div
          style={{
            borderBottom: `1px solid ${Colors.background10}`,
          }}
        >
          <div
            style={{
              width: 40,
              height: 40,
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              cursor: 'pointer',
              background: Colors.background50,
              userSelect: 'none',
            }}
          >
            <Svg.Run
              style={{
                fill: Colors.green70,
              }}
            />
          </div>
          <div
            style={{
              width: 40,
              height: 40,
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              cursor: 'not-allowed',
              background: Colors.background50,
              userSelect: 'none',
            }}
          >
            <Svg.Stop
              style={{
                fill: Colors.foreground10,
              }}
            />
          </div>
        </div>
      }
    >
      <Tabs.Pane
        id="summary"
        title={
          <Tooltip
            content="Summary"
            placement="right"
            elementStyle={{
              display: 'flex',
              padding: '12px 11px',
            }}
          >
            <Svg.Charts />
          </Tooltip>
        }
      >
        <SummaryContent />
      </Tabs.Pane>
      <Tabs.Pane
        id="queue"
        title={
          <Tooltip
            content="Tasks queue"
            placement="right"
            elementStyle={{
              display: 'flex',
              padding: '12px 11px',
            }}
          >
            <Svg.Queue />
          </Tooltip>
        }
      >
        Queue content
      </Tabs.Pane>
      <Tabs.Pane
        title={
          <Tooltip
            content="Logs"
            placement="right"
            elementStyle={{
              display: 'flex',
              padding: '12px 11px',
            }}
          >
            <Svg.Danger />
          </Tooltip>
        }
        id="logs"
      >
        Logs content
      </Tabs.Pane>
      <Tabs.Pane
        title={
          <Tooltip
            content="Key/Value"
            placement="right"
            elementStyle={{
              display: 'flex',
              padding: '12px 11px',
            }}
          >
            <Svg.KeyValue />
          </Tooltip>
        }
        id="key-value"
      >
        Key/Value content
      </Tabs.Pane>
    </Tabs>
  );
};

export default EdgeContent;
