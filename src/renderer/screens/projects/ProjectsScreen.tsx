import React from 'react';
import { Input, Svg, Tabs } from '@crawless/ui/lite';
import { Colors } from 'renderer/theme';
import styled from 'styled-components';
import ProjectTree from './tree';

const Icon = styled.div`
  height: 40px;
  width: 40px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const ProjectsScreen = () => {
  return (
    <div
      style={{ background: Colors.background10 }}
      className="w-100 d-flex flex-column"
    >
      <div
        style={{ background: Colors.background50, marginBottom: '1px' }}
        className="p-1"
      >
        <Input
          placeholder="Search..."
          className="w-100"
          iconRight={<Svg.Search />}
        />
      </div>
      <Tabs
        style={{
          height: '100%',
          background: Colors.background30,
        }}
        contentStyle={{
          borderTop: `1px solid ${Colors.background30}`,
          color: Colors.foreground70,
        }}
        paneStyle={{
          padding: '0',
        }}
        suffix={
          <Icon
            style={{
              cursor: 'not-allowed',
              background: Colors.background70,
              userSelect: 'none',
            }}
          >
            <Svg.Plus
              style={{
                fill: Colors.foreground90,
              }}
            />
          </Icon>
        }
      >
        <Tabs.Pane
          title={
            <Icon>
              <Svg.Charts />
            </Icon>
          }
          id="summary"
        >
          <ProjectTree />
        </Tabs.Pane>
        <Tabs.Pane
          title={
            <Icon>
              <Svg.Star />
            </Icon>
          }
          id="queue"
        >
          Queue content
        </Tabs.Pane>
        <Tabs.Pane
          title={
            <Icon
              style={{
                borderRight: `1px solid ${Colors.background10}`,
              }}
            >
              <Svg.Share />
            </Icon>
          }
          id="logs"
        >
          Logs
        </Tabs.Pane>
        <Tabs.Pane
          title={
            <Icon>
              <Svg.Rocket />
            </Icon>
          }
          id="rocket-value"
        >
          Rocket
        </Tabs.Pane>
        <Tabs.Pane
          title={
            <Icon>
              <Svg.Target />
            </Icon>
          }
          id="target-value"
        >
          Targets
        </Tabs.Pane>
      </Tabs>
    </div>
  );
};

export default ProjectsScreen;
