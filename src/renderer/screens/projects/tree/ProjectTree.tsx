import React, { useState, useEffect } from 'react';
import { Svg, Menu } from '@crawless/ui/lite';
import {
  FIREBASE_STORAGE,
  projects,
  MONGO_STORAGE,
  WORKFLOW,
  WORKFLOW_TASK_GROUP,
  WORKFLOW_TASK,
} from 'renderer/dataset';
import { Colors } from 'renderer/theme';
import styled from 'styled-components';

const Dot = styled.div`
  height: 5px;
  width: 5px;
  border-radius: 50%;
  background-color: ${Colors.green70};
  position: absolute;
  left: -20px;
  top: 5px;
`;

const ClearSubMenuBack = styled.div`
  .rc-menu-submenu-title {
    background: #0000;
    border-bottom: 0px !important;
  }
  .is-active-tab > .rc-menu-submenu-title {
    background: ${Colors.purple30};
  }
`;

const ProjectTree = () => {
  const renderFirebaseSubs = () => null;

  const renderMongoSubs = (el, key) => {
    const Icon = el?.Icon || Svg.Sqlite;

    return (
      <ClearSubMenuBack className="ps-2">
        <Menu.SubMenu
          key={el?.id}
          title={
            <div className="d-flex position-relative">
              <Icon
                style={{
                  fill: Colors.cyan70,
                  marginRight: 4,
                }}
              />
              <div>{el?.title || ''}</div>
            </div>
          }
          expandIcon={(props) => (
            <Svg.ArrowFilled type={props?.isOpen ? 'down' : 'right'} />
          )}
        >
          {Array.isArray(el?.subs) &&
            el.subs?.map((sub) => {
              const SubIcon = sub.Icon || Svg.Collection;

              return (
                <Menu.Item key={sub?.id} itemIcon={<SubIcon />}>
                  <span>{sub?.title}</span>
                </Menu.Item>
              );
            })}
        </Menu.SubMenu>
      </ClearSubMenuBack>
    );
  };

  const renderWorkflowSubs = (el) => {
    const Icon = el?.Icon;

    return (
      <ClearSubMenuBack className="ps-2">
        <Menu.SubMenu
          key={el?.id}
          title={
            <div className="d-flex position-relative">
              <Icon
                style={{
                  fill: Colors.cyan70,
                  marginRight: 4,
                }}
              />
              <div className="text-capitalize">{el?.title || ''}</div>
            </div>
          }
          expandIcon={(props) => (
            <Svg.ArrowFilled type={props?.isOpen ? 'down' : 'right'} />
          )}
        >
          {Array.isArray(el?.subs) &&
            el.subs?.map((sub1) => {
              const SubIcon = sub1.Icon || Svg.Collection;

              return sub1?.type === WORKFLOW_TASK_GROUP ? (
                <div className="ps-3">
                  <Menu.SubMenu
                    onClick={() => {}}
                    key={sub1?.id}
                    title={
                      <div className="d-flex position-relative">
                        <SubIcon
                          style={{
                            fill: Colors.cyan70,
                            marginRight: 4,
                          }}
                        />
                        <div>{sub1?.title || ''}</div>
                      </div>
                    }
                    expandIcon={(props) => (
                      <Svg.ArrowFilled
                        type={props?.isOpen ? 'down' : 'right'}
                      />
                    )}
                  >
                    {Array.isArray(sub1?.subs) &&
                      sub1.subs?.map((sub2) => {
                        const SubSubIcon = sub2.Icon || Svg.Collection;

                        return (
                          <Menu.Item
                            onClick={() => {}}
                            key={sub2?.id}
                            itemIcon={
                              <SubSubIcon
                                style={{ fill: sub2?.color || Colors.lime50 }}
                              />
                            }
                          >
                            <span>{sub2?.title}</span>
                          </Menu.Item>
                        );
                      })}
                  </Menu.SubMenu>
                </div>
              ) : (
                <Menu.Item
                  key={sub1?.id}
                  onClick={() => {}}
                  itemIcon={
                    <SubIcon style={{ fill: sub1?.color || Colors.lime50 }} />
                  }
                >
                  <span>{sub1?.title}</span>
                </Menu.Item>
              );
            })}
        </Menu.SubMenu>
      </ClearSubMenuBack>
    );
  };

  return (
    <div
      style={{
        width: '100%',
        background: Colors.background30,
      }}
    >
      <Menu
      /* defaultSelectedKeys={['workflow3']} */
      >
        {projects.map((project) => (
          <Menu.SubMenu
            key={project.id}
            title={
              <div className="d-flex position-relative">
                {project.isRunning && <Dot className="me-1" />}
                <Svg.Project
                  style={{
                    fill: Colors.cyan70,
                    marginRight: 4,
                  }}
                />
                <div>{project.title}</div>
              </div>
            }
            expandIcon={(props) => (
              <Svg.ArrowFilled type={props?.isOpen ? 'down' : 'right'} />
            )}
          >
            {project.subs?.map((sub1, key) => {
              if (sub1.type === FIREBASE_STORAGE) {
                return renderFirebaseSubs(sub1, key);
              }
              if (sub1.type === MONGO_STORAGE) {
                return renderMongoSubs(sub1, key);
              }
              if (sub1.type === WORKFLOW) {
                return renderWorkflowSubs(sub1, key);
              }
              return null;
            })}
          </Menu.SubMenu>
        ))}
      </Menu>
    </div>
  );
};

export default ProjectTree;
