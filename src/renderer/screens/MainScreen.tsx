/* eslint-disable promise/always-return */
/* eslint-disable promise/catch-or-return */
import React, { useState, useEffect } from 'react';
import {
  Sider,
  Svg,
  Typography,
  Switch,
  notification,
  LoadView,
} from '@crawless/ui/lite';
import { Colors } from 'renderer/theme';
import { observer } from 'mobx-react';
import HomeScreen from './home';
import ProjectsScreen, { ProjectsRenderer } from './projects';
import StoreScreen from './store';
import ArchivesScreen from './archives';
// import {observer} from "mobx-"

const HOME_SCREEN = 'HOME_SCREEN';
const PROJECTS_SCREEN = 'PROJECTS_SCREEN';
const STORE_SCREEN = 'STORE_SCREEN';
const ARCHICHE_SCREEN = 'ARCHICHE_SCREEN';
const SETTINGS_SCREEN = 'SETTINGS_SCREEN';

const MainScreen = observer(() => {
  const [activeSlider, setActiveSlider] = useState(PROJECTS_SCREEN);
  return (
    <div className="w-100-vw d-flex align-items-start">
      <Sider
        panesStyle={{ background: Colors.background10 }}
        viewStyle={{
          background: 'rgb(28, 28, 34)',
          color: 'white',
        }}
        defaultActiveId={PROJECTS_SCREEN}
        onSelect={(slider) => setActiveSlider(slider?.id)}
        panes={[
          {
            id: HOME_SCREEN,
            title: 'Home',
            icon: <Svg.Crawless size="medium" />,
            content: <HomeScreen />,
          },
          {
            id: PROJECTS_SCREEN,
            title: 'Projects',
            icon: <Svg.Crawless size="medium" />,
            content: <ProjectsScreen />,
          },
          {
            id: STORE_SCREEN,
            title: 'Store',
            icon: <Svg.Store size="medium" />,
            content: <StoreScreen />,
          },
          {
            id: ARCHICHE_SCREEN,
            title: 'Documentation',
            icon: <Svg.Book size="medium" />,
            content: <ArchivesScreen />,
          },
          {
            id: SETTINGS_SCREEN,
            title: 'Settings',
            icon: <Svg.Settings size="medium" />,
            content: <p className="stories-parag">SettingsScree</p>,
            spaceOut: true,
          },
        ]}
        suffix={
          <div
            style={{
              position: 'absolute',
              bottom: 96,
              left: 9,
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              width: 30,
            }}
          >
            <Switch
              type="warning"
              defaultChecked
              style={{
                transform: 'rotate(-90deg) translateX(16px)',
              }}
            />
            <Typography.Text
              size={20}
              style={{
                writingMode: 'tb',
                color: 'orange',
                transform: 'rotate(180deg)',
              }}
            >
              Connect to Edge
            </Typography.Text>
            <Svg.Info
              style={{
                fill: 'orange',
                marginTop: 8,
              }}
            />
          </div>
        }
      />
      <div className="text-white w-100 h-100-vh">
        {activeSlider === PROJECTS_SCREEN ? (
          <ProjectsRenderer />
        ) : null}
      </div>
    </div>
  );
});

export default MainScreen;
